package com.example.handsomefida.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "grade")
public class Grade implements Serializable{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotBlank
    private String quality;

    private Double baseProduction;
    
    @JsonIgnoreProperties
    @OneToMany(mappedBy = "grade", cascade = CascadeType.ALL)
    private Set<Publisher> publisher;

	public Grade(Long id, @NotBlank String quality, @NotBlank Double baseProduction) {
		super();
		this.id = id;
		this.quality = quality;
		this.baseProduction = baseProduction;
	}

	public Grade() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public Double getBaseProduction() {
		return baseProduction;
	}

	public void setBaseProduction(Double baseProduction) {
		this.baseProduction = baseProduction;
	}
}
