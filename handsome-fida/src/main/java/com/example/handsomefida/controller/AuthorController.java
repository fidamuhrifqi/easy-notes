package com.example.handsomefida.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.handsomefida.exception.ResourceNotFoundException;
import com.example.handsomefida.model.Author;
import com.example.handsomefida.repository.AuthorRepository;

@RestController
@RequestMapping("/api/author")
public class AuthorController {
	
	  @Autowired
	    AuthorRepository authorRepository;
	    
	    // Get All Notes
	    @GetMapping("/get")
	    public List<Author> getAllAuthors() {
	        return authorRepository.findAll();
	    }
	    
	    // Create a new Note
	    @PostMapping("/create")
	    public Author createAuthor(@Valid @RequestBody Author author) {
	        return authorRepository.save(author);
	    }
	    
	    // Get a Single Note
	    @GetMapping("/getsingle/{id}")
	    public Author getAuthorById(@PathVariable(value = "id") Long authorId) {
	        return authorRepository.findById(authorId)
	                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));
	    }
	    
	    // Update a Note
	    @PutMapping("/update/{id}")
	    public Author updateAuthor(@PathVariable(value = "id") Long authorId,
	                                            @Valid @RequestBody Author authorDetails) {

	    	Author author = authorRepository.findById(authorId)
	                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));

	    	author.setFirstName(authorDetails.getFirstName());
	    	author.setLastName(authorDetails.getLastName());

	    	Author updatedAuthor = authorRepository.save(author);
	        return updatedAuthor;
	    }
	    
	    // Delete a Note
	    @DeleteMapping("/delete/{id}")
	    public ResponseEntity<?> deleteAuthor(@PathVariable(value = "id") Long authorId) {
	    	Author author = authorRepository.findById(authorId)
	                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));

	    	authorRepository.delete(author);

	        return ResponseEntity.ok().build();
	    }
}
