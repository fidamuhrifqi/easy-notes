package com.example.handsomefida.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.handsomefida.exception.ResourceNotFoundException;
import com.example.handsomefida.model.Book;
import com.example.handsomefida.repository.BookRepository;

@RestController
@RequestMapping("/api/books")
public class BookController {
	
    @Autowired
    BookRepository bookRepository;
    
    // Get All Notes
    @GetMapping("/get")
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }
    
    // Create a new Note
    @PostMapping("/create")
    public Book createBook(@Valid @RequestBody Book book) {
        return bookRepository.save(book);
    }
    
    // Get a Single Note
    @GetMapping("/getsingle/{id}")
    public Book getBookById(@PathVariable(value = "id") Long bookId) {
        return bookRepository.findById(bookId)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
    }
    
    // Update a Note
    @PutMapping("/update/{id}")
    public Book updateBook(@PathVariable(value = "id") Long bookId,
                                            @Valid @RequestBody Book bookDetails) {

        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));

        book.setTitle(bookDetails.getTitle());
        book.setDescription(bookDetails.getDescription());
        book.setReleaseDate(bookDetails.getReleaseDate());
        book.setAuthor(bookDetails.getAuthor());
        book.setPublisher(bookDetails.getPublisher());
        book.setCategory(bookDetails.getCategory());

        Book updatedBook = bookRepository.save(book);
        return updatedBook;
    }
    
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable(value = "id") Long bookId) {
    	Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));

    	bookRepository.delete(book);

        return ResponseEntity.ok().build();
    }
}
