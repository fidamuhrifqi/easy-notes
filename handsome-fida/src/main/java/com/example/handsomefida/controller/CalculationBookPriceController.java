package com.example.handsomefida.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.handsomefida.exception.ResourceNotFoundException;
import com.example.handsomefida.model.Book;
import com.example.handsomefida.repository.BookRepository;

@RestController
@RequestMapping("/api/calculation/book/price")
public class CalculationBookPriceController {
    
	@Autowired
    BookRepository bookRepository;


    // Update a Book Price
    @PutMapping("/update")
    public HashMap<String, Object> calculationBookPrice() {
    	
    	List<Book> getAllBooks = bookRepository.findAll();
    	
    	for (Book bookFromList : getAllBooks) {
    		
    		Book book = bookFromList;
            double bookPrice;
            
            bookPrice = 1.5 * book.getPublisher().getGrade().getBaseProduction();
            
            book.setPrice(bookPrice);
            bookRepository.save(book);
		}
    	
    	HashMap<String, Object> message = new HashMap<String, Object>();
    	message.put("Status", 200);
    	message.put("Message", "BookPrice Calculation Is Success");

    	
		return message;
    }
}
