package com.example.handsomefida;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class HandsomeFidaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HandsomeFidaApplication.class, args);
	}

}
